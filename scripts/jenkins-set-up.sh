#!/usr/bin/env bash
source scripts/env_variables.sh

oc project "$IMAGE_STREAM_NAMESPACE"

oc process -f templates/jenkins-s2i.yml --param-file=templates/jenkins_var.env | oc create -f -


#oc delete is/jenkins
#oc delete is/jenkins-custom
#oc delete secret/bitbucket-auth
#oc delete all -l test=test
