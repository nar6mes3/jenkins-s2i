#!/usr/bin/env bash
SOURCE_REPOSITORY_URL=https://nar6mes3@bitbucket.org/nar6mes3/jenkins-s2i.git
NAME=jenkins-custom
SOURCE_REPOSITORY_CONTEXT_DIR=jenkins-s2i
IMAGE_STREAM_NAMESPACE=nsmr-ci-cd
SOURCE_REPOSITORY_SECRET=bitbucket-secret
